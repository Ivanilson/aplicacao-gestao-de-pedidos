object FrmManuPedido: TFrmManuPedido
  Left = 0
  Top = 0
  Caption = 'FrmManuPedido'
  ClientHeight = 617
  ClientWidth = 816
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 816
    Height = 519
    Align = alClient
    TabOrder = 0
    ExplicitHeight = 628
    DesignSize = (
      816
      519)
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 119
      Height = 19
      Caption = 'Pedido de Venda'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 199
      Width = 111
      Height = 19
      Caption = 'Itens do Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 399
      Width = 93
      Height = 13
      Caption = 'C'#243'digo do Produto:'
    end
    object Label4: TLabel
      Left = 472
      Top = 391
      Width = 60
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label5: TLabel
      Left = 656
      Top = 391
      Width = 64
      Height = 13
      Caption = 'Valor Unit'#225'rio'
    end
    object Label6: TLabel
      Left = 20
      Top = 113
      Width = 77
      Height = 16
      Caption = 'Data Emiss'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbNomeCli: TLabel
      Left = 252
      Top = 75
      Width = 77
      Height = 16
      Caption = 'Data Emiss'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbProduto: TLabel
      Left = 141
      Top = 414
      Width = 66
      Height = 16
      Caption = 'NOMEPROD'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 21
      Top = 52
      Width = 100
      Height = 16
      Caption = 'C'#243'digo do Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 194
      Top = 19
      Width = 105
      Height = 16
      Caption = 'N'#250'mero do Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 141
      Top = 399
      Width = 95
      Height = 13
      Caption = 'Produto Pesquisado'
    end
    object DBGrid2: TDBGrid
      Left = 16
      Top = 224
      Width = 761
      Height = 161
      DataSource = DmGeral.DsTempItensPedido
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnKeyDown = DBGrid2KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'NUMERO_PEDIDO'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'CODIGO_PRODUTO'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'DESCRICAO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTIDADE'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR_UNITARIO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR_TOTAL'
          Visible = True
        end>
    end
    object BbNovo: TButton
      AlignWithMargins = True
      Left = 14
      Top = 477
      Width = 91
      Height = 25
      Anchors = []
      Caption = '&Novo Item'
      TabOrder = 1
      OnClick = BbNovoClick
    end
    object BbSalvar: TButton
      AlignWithMargins = True
      Left = 141
      Top = 477
      Width = 91
      Height = 25
      Anchors = []
      Caption = '&Salvar Item'
      TabOrder = 2
      OnClick = BbSalvarClick
    end
    object BbBuscarProduto: TButton
      AlignWithMargins = True
      Left = 14
      Top = 440
      Width = 91
      Height = 25
      Anchors = []
      Caption = '&Buscar Produto'
      TabOrder = 3
      OnClick = BbBuscarProdutoClick
    end
    object EdtEmissao: TDBEdit
      Left = 20
      Top = 135
      Width = 119
      Height = 21
      DataField = 'DATAEMISSAO'
      DataSource = DmGeral.DsTempPedido
      TabOrder = 4
    end
    object BbBuscaCliente: TButton
      Left = 117
      Top = 72
      Width = 119
      Height = 25
      Caption = 'Buscar Cliente'
      TabOrder = 5
      OnClick = BbBuscaClienteClick
    end
    object EdtProduto: TDBEdit
      Left = 14
      Top = 418
      Width = 121
      Height = 21
      DataField = 'CODIGO_PRODUTO'
      DataSource = DmGeral.DsTempItensPedido
      TabOrder = 6
    end
    object EdtQuantidade: TDBEdit
      Left = 472
      Top = 410
      Width = 121
      Height = 21
      DataField = 'QUANTIDADE'
      DataSource = DmGeral.DsTempItensPedido
      TabOrder = 7
    end
    object EdtValUnitario: TDBEdit
      Left = 656
      Top = 410
      Width = 121
      Height = 21
      DataField = 'VALOR_UNITARIO'
      DataSource = DmGeral.DsTempItensPedido
      TabOrder = 8
    end
    object BbDescartar: TButton
      AlignWithMargins = True
      Left = 240
      Top = 477
      Width = 91
      Height = 25
      Anchors = []
      Caption = '&Descartar Item'
      TabOrder = 9
      OnClick = BbDescartarClick
    end
    object EdtCodCliente: TDBEdit
      Left = 20
      Top = 74
      Width = 91
      Height = 21
      DataField = 'CODIGO_CLIENTE'
      DataSource = DmGeral.DsTempPedido
      Enabled = False
      TabOrder = 10
    end
    object EdtNumeroPedido: TDBEdit
      Left = 305
      Top = 18
      Width = 91
      Height = 21
      DataField = 'NUMERO_PEDIDO'
      DataSource = DmGeral.DsTempPedido
      Enabled = False
      TabOrder = 11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 519
    Width = 816
    Height = 98
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 584
    DesignSize = (
      816
      98)
    object Label8: TLabel
      Left = 656
      Top = 72
      Width = 68
      Height = 16
      Anchors = [akRight, akBottom]
      Caption = 'Valor Total:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbBusca: TLabel
      Left = 468
      Top = 14
      Width = 81
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Pesquisar Pedido'
    end
    object BbFechar: TButton
      AlignWithMargins = True
      Left = 713
      Top = 6
      Width = 91
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Fechar'
      TabOrder = 0
      OnClick = BbFecharClick
    end
    object BbSalvarPed: TButton
      AlignWithMargins = True
      Left = 237
      Top = 55
      Width = 91
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Salvar Pedido'
      TabOrder = 1
      OnClick = BbSalvarPedClick
    end
    object EdtValorTotal: TDBEdit
      Left = 730
      Top = 72
      Width = 74
      Height = 21
      Anchors = [akRight, akBottom]
      DataField = 'VALOR_TOTAL'
      DataSource = DmGeral.DsTempPedido
      ReadOnly = True
      TabOrder = 2
    end
    object BbExcPed: TButton
      Left = 141
      Top = 22
      Width = 91
      Height = 25
      Caption = 'Excluir Pedido'
      TabOrder = 3
      OnClick = BbExcPedClick
    end
    object EdtPedido: TEdit
      Left = 555
      Top = 6
      Width = 70
      Height = 21
      Anchors = [akRight, akBottom]
      NumbersOnly = True
      TabOrder = 4
    end
    object BbBuscaPedido: TButton
      Left = 631
      Top = 6
      Width = 76
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Buscar'
      TabOrder = 5
      OnClick = BbBuscaPedidoClick
    end
    object BbNovoPed: TButton
      Left = 14
      Top = 22
      Width = 90
      Height = 25
      Caption = 'Novo Pedido'
      TabOrder = 6
      OnClick = BbNovoPedClick
    end
    object BbDescartarPed: TButton
      AlignWithMargins = True
      Left = 334
      Top = 55
      Width = 91
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Descartar Pedido'
      TabOrder = 7
      OnClick = BbDescartarPedClick
    end
    object BbAltPed: TButton
      Left = 14
      Top = 53
      Width = 90
      Height = 25
      Caption = 'Alterar Pedido'
      TabOrder = 8
      OnClick = BbAltPedClick
    end
  end
end
