unit uPedidos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids;

type

  TFrmPedidos = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    BbFechar: TButton;
    BbNovoPedido: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BbFecharClick(Sender: TObject);
    procedure BbNovoPedidoClick(Sender: TObject);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
     procedure ExcluirItensPedidoBanco ( iNumeroPedido : integer);
     procedure AtualizaTotal;
  end;



var
  FrmPedidos: TFrmPedidos;

implementation

{$R *.dfm}

uses uDmGeral, uBuscaCliente, uMain, uManuPedido;

procedure TFrmPedidos.BbFecharClick(Sender: TObject);
begin
  Close
end;

procedure TFrmPedidos.BbNovoPedidoClick(Sender: TObject);
begin
  // Abre Tela Manuten��o de Pedidos (incluir, alterar e excluir)
  Try
    FrmManuPedido    :=   TFrmManuPedido.Create(Application);
    FrmManuPedido.ShowModal;
  Finally
    FrmManuPedido.Free
  End;

  // Faz refresh nos datasets
  DmGeral.QrPedido.Close;
  With DmGeral.QrPedido do
    Begin
       SQL.Clear;
       SQL.Add('SELECT PEDIDO.NUMERO_PEDIDO, PEDIDO.DATA_EMISSAO, PEDIDO.CODIGO_CLIENTE,');
       SQL.Add('CLIENTES.NOME, PEDIDO.VALOR_TOTAL');
       SQL.Add('FROM pedidos_dados_gerais PEDIDO');
       SQL.Add('INNER JOIN clientes ON (PEDIDO.CODIGO_CLIENTE = clientes.CODIGO_CLIENTE)');
       Open;
    End;

  // Abre Query lista itens do pedido (detalhe)
  With DmGeral.QrItensPedido do
    Begin
       SQL.Clear;
       SQL.Add('SELECT');
       SQL.Add('PEDIDO.CODIGO_ITEM, PEDIDO.NUMERO_PEDIDO, PEDIDO.CODIGO_PRODUTO,');
       SQL.Add('PEDIDO.QUANTIDADE, produtos.DESCRICAO,');
       SQL.Add('PEDIDO.VALOR_UNITARIO, PEDIDO.VALOR_TOTAL');
       SQL.Add('FROM pedidos_produtos PEDIDO');
       SQL.Add('INNER JOIN produtos ON (PEDIDO.CODIGO_PRODUTO = produtos.CODIGO_PRODUTO)');
       SQL.Add('WHERE PEDIDO.NUMERO_PEDIDO = :NUMERO_PEDIDO');
       Open;
    End;

end;

procedure TFrmPedidos.DBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = VK_DELETE then
  begin
    if ( not DmGeral.TempItensPedido.IsEmpty) then
      Begin
       if MessageDlg('Confirma exclus�o do item do pedido?',mtConfirmation,[mbYes,MbNo],0) = mrYes then
         begin
         // Poe em modo de edi��o o registro
         DmGeral.QrIncNumeroPedido.Delete;
         AtualizaTotal;
         end;
      End;

  end;

end;

procedure TFrmPedidos.ExcluirItensPedidoBanco(iNumeroPedido: integer);
begin

end;

procedure TFrmPedidos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   DmGeral.QrPedido.Close;
   DmGeral.QrItensPedido.Close;
end;

procedure TFrmPedidos.FormShow(Sender: TObject);
begin
  Caption :=   'Pedidos de Venda';

  // Entra listando os pedidos atuais

  DmGeral.QrPedido.Close;
  With DmGeral.QrPedido do
    Begin
       SQL.Clear;
       SQL.Add('SELECT PEDIDO.NUMERO_PEDIDO, PEDIDO.DATA_EMISSAO, PEDIDO.CODIGO_CLIENTE,');
       SQL.Add('CLIENTES.NOME, PEDIDO.VALOR_TOTAL');
       SQL.Add('FROM pedidos_dados_gerais PEDIDO');
       SQL.Add('INNER JOIN clientes ON (PEDIDO.CODIGO_CLIENTE = clientes.CODIGO_CLIENTE)');
       Open;
    End;

  // Abre Query lista itens do pedido (detalhe)
  With DmGeral.QrItensPedido do
    Begin
       SQL.Clear;
       SQL.Add('SELECT');
       SQL.Add('PEDIDO.CODIGO_ITEM, PEDIDO.NUMERO_PEDIDO, PEDIDO.CODIGO_PRODUTO,');
       SQL.Add('PEDIDO.QUANTIDADE, produtos.DESCRICAO,');
       SQL.Add('PEDIDO.VALOR_UNITARIO, PEDIDO.VALOR_TOTAL');
       SQL.Add('FROM pedidos_produtos PEDIDO');
       SQL.Add('INNER JOIN produtos ON (PEDIDO.CODIGO_PRODUTO = produtos.CODIGO_PRODUTO)');
       SQL.Add('WHERE PEDIDO.NUMERO_PEDIDO = :NUMERO_PEDIDO');
       Open;
    End;

end;

procedure TFrmPedidos.AtualizaTotal;
var dTotal : double;
begin
  // Soma Valor Total dos Itens
  dTotal := 0;
  // Soma os itens do pedido e atualiza total
  DmGeral.QrItensPedido.First;
  while (not DmGeral.QrItensPedido.Eof ) do
     begin
        dTotal :=  dTotal + DmGeral.QrItensPedidoVALOR_TOTAL.Value;
        DmGeral.QrItensPedido.Next;
     end;
  // Grava campo Valor total do Pedido
  DmGeral.QrPedido.Edit;
  DmGeral.QrPedidoVALOR_TOTAL.Value :=  dTotal;
  DmGeral.QrPedido.Post;
end;


end.
