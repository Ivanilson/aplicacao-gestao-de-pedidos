unit uDmGeral;

interface

uses
  System.SysUtils, System.Classes, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Datasnap.DBClient;

type
  TDmGeral = class(TDataModule)
    FConexao: TFDConnection;
    FTrs: TFDTransaction;
    QrPedido: TFDQuery;
    DsPedido: TDataSource;
    QrPedidoNUMERO_PEDIDO: TFDAutoIncField;
    QrPedidoDATA_EMISSAO: TDateField;
    QrPedidoCODIGO_CLIENTE: TIntegerField;
    QrPedidoNOME: TStringField;
    QrPedidoVALOR_TOTAL: TBCDField;
    QrItensPedido: TFDQuery;
    DsItens: TDataSource;
    QrInserePedido: TFDQuery;
    UpdatePedido: TFDUpdateSQL;
    DsTempPedido: TDataSource;
    QrBuscaCliente: TFDQuery;
    DsBuscaCliente: TDataSource;
    QrBuscaClienteCODIGO_CLIENTE: TFDAutoIncField;
    QrBuscaClienteNOME: TStringField;
    QrBuscaClienteCIDADE: TStringField;
    QrBuscaClienteUF: TStringField;
    QrBuscaProduto: TFDQuery;
    QrBuscaProdutoCODIGO_PRODUTO: TFDAutoIncField;
    QrBuscaProdutoDESCRICAO: TStringField;
    QrBuscaProdutoPRECO_VENDA: TBCDField;
    DsTempItensPedido: TDataSource;
    QrNumeroPedido: TFDQuery;
    QrInsereItemPedido: TFDQuery;
    UpdItensPedido: TFDUpdateSQL;
    TInsPedido: TFDTransaction;
    TInsItemPedido: TFDTransaction;
    QrNumeroPedidoNUMERO_PROX_PEDIDO: TIntegerField;
    QrIncNumeroPedido: TFDQuery;
    IntegerField1: TIntegerField;
    TrsIncNumeroPedido: TFDTransaction;
    TrsNumeroPedido: TFDTransaction;
    QrItensPedidoCODIGO_ITEM: TFDAutoIncField;
    QrItensPedidoNUMERO_PEDIDO: TIntegerField;
    QrItensPedidoCODIGO_PRODUTO: TIntegerField;
    QrItensPedidoQUANTIDADE: TIntegerField;
    QrItensPedidoDESCRICAO: TStringField;
    QrItensPedidoVALOR_UNITARIO: TBCDField;
    QrItensPedidoVALOR_TOTAL: TBCDField;
    TempItensPedido: TClientDataSet;
    TempItensPedidoNUMERO_PEDIDO: TIntegerField;
    TempItensPedidoCODIGO_PRODUTO: TIntegerField;
    TempItensPedidoDESCRICAO: TStringField;
    TempItensPedidoVALOR_UNITARIO: TCurrencyField;
    TempItensPedidoVALOR_TOTAL: TCurrencyField;
    TempItensPedidoQUANTIDADE: TIntegerField;
    TempPedido: TClientDataSet;
    TempPedidoNUMERO_PEDIDO: TIntegerField;
    TempPedidoCODIGO_CLIENTE: TIntegerField;
    TempPedidoNOMECLIENTE: TStringField;
    TempPedidoDATAEMISSAO: TDateField;
    TempPedidoVALOR_TOTAL: TCurrencyField;
    QrExcluirItemPedidoBanco: TFDQuery;
    IntegerField2: TIntegerField;
    TrsExcluirItemPedidoBanco: TFDTransaction;
    QrExcluirPedidoBanco: TFDQuery;
    IntegerField3: TIntegerField;
    TrsExcluirPedidoBanco: TFDTransaction;
    QrAtualizaPedidoBanco: TFDQuery;
    IntegerField4: TIntegerField;
    TrsAtualizaPedidoBanco: TFDTransaction;
    procedure TempPedidoAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DmGeral: TDmGeral;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDmGeral.TempPedidoAfterInsert(DataSet: TDataSet);
begin
  TempPedidoDATAEMISSAO.Value := Date;
  TempPedidoVALOR_TOTAL.Value := 0;
end;

end.
