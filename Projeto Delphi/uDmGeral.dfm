object DmGeral: TDmGeral
  OldCreateOrder = False
  Height = 449
  Width = 817
  object FConexao: TFDConnection
    Params.Strings = (
      'Database=wk'
      'User_Name=root'
      'DriverID=MySQL')
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    LoginPrompt = False
    Transaction = FTrs
    Left = 56
    Top = 24
  end
  object FTrs: TFDTransaction
    Options.AutoStart = False
    Options.AutoStop = False
    Connection = FConexao
    Left = 48
    Top = 128
  end
  object QrPedido: TFDQuery
    Connection = FConexao
    Transaction = TInsPedido
    UpdateTransaction = FTrs
    SQL.Strings = (
      
        'SELECT PEDIDO.NUMERO_PEDIDO, PEDIDO.DATA_EMISSAO, PEDIDO.CODIGO_' +
        'CLIENTE, '
      'CLIENTES.NOME, PEDIDO.VALOR_TOTAL'
      'FROM pedidos_dados_gerais PEDIDO '
      
        'INNER JOIN clientes ON (PEDIDO.CODIGO_CLIENTE = clientes.CODIGO_' +
        'CLIENTE)')
    Left = 200
    Top = 24
    object QrPedidoNUMERO_PEDIDO: TFDAutoIncField
      DisplayLabel = 'Pedido'
      FieldName = 'NUMERO_PEDIDO'
      Origin = 'NUMERO_PEDIDO'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object QrPedidoDATA_EMISSAO: TDateField
      DisplayLabel = 'Data Emiss'#227'o'
      FieldName = 'DATA_EMISSAO'
      Origin = 'DATA_EMISSAO'
      Required = True
    end
    object QrPedidoCODIGO_CLIENTE: TIntegerField
      FieldName = 'CODIGO_CLIENTE'
      Origin = 'CODIGO_CLIENTE'
      Required = True
    end
    object QrPedidoNOME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cliente'
      FieldName = 'NOME'
      Origin = 'NOME'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object QrPedidoVALOR_TOTAL: TBCDField
      DisplayLabel = 'Total'
      FieldName = 'VALOR_TOTAL'
      Origin = 'VALOR_TOTAL'
      Required = True
      DisplayFormat = '#,0.00'
      Precision = 12
      Size = 2
    end
  end
  object DsPedido: TDataSource
    DataSet = QrPedido
    Left = 304
    Top = 24
  end
  object QrItensPedido: TFDQuery
    MasterSource = DsPedido
    MasterFields = 'NUMERO_PEDIDO'
    DetailFields = 'NUMERO_PEDIDO'
    Connection = FConexao
    Transaction = TInsItemPedido
    UpdateTransaction = FTrs
    FetchOptions.AssignedValues = [evCache]
    FetchOptions.Cache = [fiBlobs, fiMeta]
    UpdateObject = UpdItensPedido
    SQL.Strings = (
      'SELECT'
      
        'PEDIDO.CODIGO_ITEM, PEDIDO.NUMERO_PEDIDO, PEDIDO.CODIGO_PRODUTO,' +
        ' '
      'PEDIDO.QUANTIDADE, produtos.DESCRICAO,'
      'PEDIDO.VALOR_UNITARIO, PEDIDO.VALOR_TOTAL '
      'FROM pedidos_produtos PEDIDO '
      
        'INNER JOIN produtos ON (PEDIDO.CODIGO_PRODUTO = produtos.CODIGO_' +
        'PRODUTO)'
      'WHERE PEDIDO.NUMERO_PEDIDO = :NUMERO_PEDIDO')
    Left = 200
    Top = 128
    ParamData = <
      item
        Name = 'NUMERO_PEDIDO'
        DataType = ftAutoInc
        ParamType = ptInput
        Value = Null
      end>
    object QrItensPedidoCODIGO_ITEM: TFDAutoIncField
      FieldName = 'CODIGO_ITEM'
      Origin = 'CODIGO_ITEM'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object QrItensPedidoNUMERO_PEDIDO: TIntegerField
      FieldName = 'NUMERO_PEDIDO'
      Origin = 'NUMERO_PEDIDO'
      Required = True
    end
    object QrItensPedidoCODIGO_PRODUTO: TIntegerField
      FieldName = 'CODIGO_PRODUTO'
      Origin = 'CODIGO_PRODUTO'
      Required = True
    end
    object QrItensPedidoDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Produto'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object QrItensPedidoVALOR_UNITARIO: TBCDField
      DisplayLabel = 'Vl. Unit'#225'rio'
      FieldName = 'VALOR_UNITARIO'
      Origin = 'VALOR_UNITARIO'
      Required = True
      DisplayFormat = '#,0.00'
      Precision = 12
      Size = 2
    end
    object QrItensPedidoQUANTIDADE: TIntegerField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
      Origin = 'QUANTIDADE'
      Required = True
    end
    object QrItensPedidoVALOR_TOTAL: TBCDField
      DisplayLabel = 'Valor Total'
      FieldName = 'VALOR_TOTAL'
      Origin = 'VALOR_TOTAL'
      Required = True
      DisplayFormat = '#,0.00'
      Precision = 12
      Size = 2
    end
  end
  object DsItens: TDataSource
    DataSet = QrItensPedido
    Left = 304
    Top = 128
  end
  object QrInserePedido: TFDQuery
    Connection = FConexao
    Transaction = TInsPedido
    SQL.Strings = (
      'INSERT INTO wk.pedidos_dados_gerais'
      '(numero_pedido, data_emissao, codigo_cliente, valor_total)'
      
        'VALUES (:new_numero_pedido, :new_data_emissao, :new_codigo_clien' +
        'te, :new_valor_total)')
    Left = 136
    Top = 264
    ParamData = <
      item
        Name = 'NEW_NUMERO_PEDIDO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'NEW_DATA_EMISSAO'
        DataType = ftDate
        FDDataType = dtDate
        ParamType = ptInput
      end
      item
        Name = 'NEW_CODIGO_CLIENTE'
        DataType = ftInteger
        FDDataType = dtUInt16
        ParamType = ptInput
      end
      item
        Name = 'NEW_VALOR_TOTAL'
        DataType = ftCurrency
        FDDataType = dtCurrency
        ParamType = ptInput
      end>
  end
  object UpdatePedido: TFDUpdateSQL
    Connection = FConexao
    InsertSQL.Strings = (
      'INSERT INTO wk.pedidos_dados_gerais'
      '(data_emissao, codigo_cliente, valor_total)'
      
        'VALUES (:new_data_emissao, :new_codigo_cliente, :new_valor_total' +
        ')')
    ModifySQL.Strings = (
      'UPDATE wk.pedidos_dados_gerais'
      
        'SET numero_pedido = :new_numero_pedido, data_emissao = :new_data' +
        '_emissao, '
      
        '  codigo_cliente = :new_codigo_cliente, valor_total = :new_valor' +
        '_total'
      'WHERE numero_pedido = :old_numero_pedido')
    DeleteSQL.Strings = (
      'DELETE FROM wk.pedidos_dados_gerais'
      'WHERE numero_pedido = :old_numero_pedido')
    FetchRowSQL.Strings = (
      'SELECT numero_pedido, data_emissao, codigo_cliente, valor_total'
      'FROM wk.pedidos_dados_gerais'
      'WHERE numero_pedido = :old_numero_pedido')
    Left = 416
    Top = 24
  end
  object DsTempPedido: TDataSource
    DataSet = TempPedido
    Left = 648
    Top = 128
  end
  object QrBuscaCliente: TFDQuery
    Connection = FConexao
    Transaction = FTrs
    UpdateTransaction = FTrs
    SQL.Strings = (
      'SELECT * FROM CLIENTES'
      'WHERE NOME LIKE :PNOME'
      'ORDER BY NOME')
    Left = 600
    Top = 264
    ParamData = <
      item
        Name = 'PNOME'
        DataType = ftString
        ParamType = ptInput
      end>
    object QrBuscaClienteCODIGO_CLIENTE: TFDAutoIncField
      FieldName = 'CODIGO_CLIENTE'
      Origin = 'CODIGO_CLIENTE'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object QrBuscaClienteNOME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 50
    end
    object QrBuscaClienteCIDADE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CIDADE'
      Origin = 'CIDADE'
      Size = 50
    end
    object QrBuscaClienteUF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'UF'
      Origin = 'UF'
      Size = 2
    end
  end
  object DsBuscaCliente: TDataSource
    DataSet = QrBuscaCliente
    Left = 704
    Top = 264
  end
  object QrBuscaProduto: TFDQuery
    Connection = FConexao
    Transaction = FTrs
    UpdateTransaction = FTrs
    SQL.Strings = (
      'SELECT * FROM PRODUTOS'
      'WHERE CODIGO_PRODUTO = :PCODIGO')
    Left = 600
    Top = 320
    ParamData = <
      item
        Name = 'PCODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object QrBuscaProdutoCODIGO_PRODUTO: TFDAutoIncField
      FieldName = 'CODIGO_PRODUTO'
      Origin = 'CODIGO_PRODUTO'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object QrBuscaProdutoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 50
    end
    object QrBuscaProdutoPRECO_VENDA: TBCDField
      FieldName = 'PRECO_VENDA'
      Origin = 'PRECO_VENDA'
      Required = True
      Precision = 12
      Size = 2
    end
  end
  object DsTempItensPedido: TDataSource
    DataSet = TempItensPedido
    Left = 648
    Top = 200
  end
  object QrNumeroPedido: TFDQuery
    Connection = FConexao
    Transaction = TrsNumeroPedido
    SQL.Strings = (
      'SELECT * FROM param_pedido')
    Left = 248
    Top = 264
    object QrNumeroPedidoNUMERO_PROX_PEDIDO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO_PROX_PEDIDO'
      Origin = 'NUMERO_PROX_PEDIDO'
    end
  end
  object QrInsereItemPedido: TFDQuery
    Connection = FConexao
    Transaction = TInsItemPedido
    SQL.Strings = (
      'INSERT INTO wk.pedidos_produtos'
      '(numero_pedido, codigo_produto, quantidade, valor_unitario, '
      '  valor_total)'
      
        'VALUES (:new_numero_pedido, :new_codigo_produto, :new_quantidade' +
        ', :new_valor_unitario, '
      '  :new_valor_total)')
    Left = 136
    Top = 336
    ParamData = <
      item
        Name = 'NEW_NUMERO_PEDIDO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'NEW_CODIGO_PRODUTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'NEW_QUANTIDADE'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'NEW_VALOR_UNITARIO'
        DataType = ftCurrency
        ParamType = ptInput
      end
      item
        Name = 'NEW_VALOR_TOTAL'
        DataType = ftCurrency
        FDDataType = dtCurrency
        ParamType = ptInput
      end>
  end
  object UpdItensPedido: TFDUpdateSQL
    Connection = FConexao
    InsertSQL.Strings = (
      'INSERT INTO wk.pedidos_dados_gerais'
      '(data_emissao, codigo_cliente, valor_total)'
      
        'VALUES (:new_data_emissao, :new_codigo_cliente, :new_valor_total' +
        ')')
    ModifySQL.Strings = (
      'UPDATE wk.pedidos_dados_gerais'
      
        'SET numero_pedido = :new_numero_pedido, data_emissao = :new_data' +
        '_emissao, '
      
        '  codigo_cliente = :new_codigo_cliente, valor_total = :new_valor' +
        '_total'
      'WHERE numero_pedido = :old_numero_pedido')
    DeleteSQL.Strings = (
      'DELETE FROM wk.pedidos_dados_gerais'
      'WHERE numero_pedido = :old_numero_pedido')
    FetchRowSQL.Strings = (
      'SELECT numero_pedido, data_emissao, codigo_cliente, valor_total'
      'FROM wk.pedidos_dados_gerais'
      'WHERE numero_pedido = :old_numero_pedido')
    Left = 416
    Top = 128
  end
  object TInsPedido: TFDTransaction
    Connection = FConexao
    Left = 24
    Top = 264
  end
  object TInsItemPedido: TFDTransaction
    Connection = FConexao
    Left = 32
    Top = 336
  end
  object QrIncNumeroPedido: TFDQuery
    Connection = FConexao
    Transaction = TrsIncNumeroPedido
    SQL.Strings = (
      'UPDATE  param_pedido'
      'SET NUMERO_PROX_PEDIDO = NUMERO_PROX_PEDIDO + 1')
    Left = 248
    Top = 328
    object IntegerField1: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO_PROX_PEDIDO'
      Origin = 'NUMERO_PROX_PEDIDO'
    end
  end
  object TrsIncNumeroPedido: TFDTransaction
    Connection = FConexao
    Left = 352
    Top = 328
  end
  object TrsNumeroPedido: TFDTransaction
    Options.AutoStart = False
    Options.AutoStop = False
    Connection = FConexao
    Left = 352
    Top = 264
  end
  object TempItensPedido: TClientDataSet
    PersistDataPacket.Data = {
      CB0000009619E0BD010000001800000006000000000003000000CB000D4E554D
      45524F5F50454449444F04000100000000000E434F4449474F5F50524F445554
      4F04000100000000000944455343524943414F01004900000001000557494454
      480200020032000E56414C4F525F554E49544152494F08000400000001000753
      5542545950450200490006004D6F6E6579000B56414C4F525F544F54414C0800
      04000000010007535542545950450200490006004D6F6E6579000A5155414E54
      494441444504000100000000000000}
    Active = True
    Aggregates = <>
    Params = <>
    Left = 520
    Top = 200
    object TempItensPedidoNUMERO_PEDIDO: TIntegerField
      FieldName = 'NUMERO_PEDIDO'
    end
    object TempItensPedidoCODIGO_PRODUTO: TIntegerField
      FieldName = 'CODIGO_PRODUTO'
    end
    object TempItensPedidoDESCRICAO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object TempItensPedidoVALOR_UNITARIO: TCurrencyField
      DisplayLabel = 'Valor Unit'#225'rio'
      FieldName = 'VALOR_UNITARIO'
      DisplayFormat = '#,0.00'
      currency = False
    end
    object TempItensPedidoVALOR_TOTAL: TCurrencyField
      DisplayLabel = 'Total'
      FieldName = 'VALOR_TOTAL'
      DisplayFormat = '#,0.00'
      currency = False
    end
    object TempItensPedidoQUANTIDADE: TIntegerField
      DisplayLabel = 'Quantidade'
      FieldName = 'QUANTIDADE'
    end
  end
  object TempPedido: TClientDataSet
    PersistDataPacket.Data = {
      A30000009619E0BD010000001800000005000000000003000000A3000D4E554D
      45524F5F50454449444F04000100000000000E434F4449474F5F434C49454E54
      4504000100000000000B4E4F4D45434C49454E54450100490000000100055749
      4454480200020032000B44415441454D495353414F04000600000000000B5641
      4C4F525F544F54414C080004000000010007535542545950450200490006004D
      6F6E6579000000}
    Active = True
    Aggregates = <>
    Params = <>
    AfterInsert = TempPedidoAfterInsert
    Left = 520
    Top = 120
    object TempPedidoNUMERO_PEDIDO: TIntegerField
      FieldName = 'NUMERO_PEDIDO'
    end
    object TempPedidoCODIGO_CLIENTE: TIntegerField
      FieldName = 'CODIGO_CLIENTE'
    end
    object TempPedidoNOMECLIENTE: TStringField
      FieldName = 'NOMECLIENTE'
      Size = 50
    end
    object TempPedidoDATAEMISSAO: TDateField
      FieldName = 'DATAEMISSAO'
    end
    object TempPedidoVALOR_TOTAL: TCurrencyField
      FieldName = 'VALOR_TOTAL'
      DisplayFormat = '#,0.00'
      currency = False
    end
  end
  object QrExcluirItemPedidoBanco: TFDQuery
    Connection = FConexao
    Transaction = TrsExcluirItemPedidoBanco
    SQL.Strings = (
      'DELETE FROM pedidos_produtos'
      'WHERE  NUMERO_PEDIDO = :NPEDIDO')
    Left = 40
    Top = 392
    ParamData = <
      item
        Name = 'NPEDIDO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object IntegerField2: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO_PROX_PEDIDO'
      Origin = 'NUMERO_PROX_PEDIDO'
    end
  end
  object TrsExcluirItemPedidoBanco: TFDTransaction
    Connection = FConexao
    Left = 176
    Top = 392
  end
  object QrExcluirPedidoBanco: TFDQuery
    Connection = FConexao
    Transaction = TrsExcluirPedidoBanco
    SQL.Strings = (
      'DELETE FROM pedidos_dados_gerais'
      'WHERE  NUMERO_PEDIDO = :NPEDIDO')
    Left = 656
    Top = 376
    ParamData = <
      item
        Name = 'NPEDIDO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object IntegerField3: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO_PROX_PEDIDO'
      Origin = 'NUMERO_PROX_PEDIDO'
    end
  end
  object TrsExcluirPedidoBanco: TFDTransaction
    Connection = FConexao
    Left = 744
    Top = 376
  end
  object QrAtualizaPedidoBanco: TFDQuery
    Connection = FConexao
    Transaction = TrsAtualizaPedidoBanco
    SQL.Strings = (
      'UPDATE wk.pedidos_dados_gerais'
      'SET '
      '  data_emissao = :new_data_emissao, '
      '  codigo_cliente = :new_codigo_cliente, '
      '  valor_total = :new_valor_total'
      'WHERE numero_pedido = :pnumero_pedido')
    Left = 400
    Top = 384
    ParamData = <
      item
        Name = 'NEW_DATA_EMISSAO'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'NEW_CODIGO_CLIENTE'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'NEW_VALOR_TOTAL'
        DataType = ftCurrency
        ParamType = ptInput
      end
      item
        Name = 'PNUMERO_PEDIDO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object IntegerField4: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'NUMERO_PROX_PEDIDO'
      Origin = 'NUMERO_PROX_PEDIDO'
    end
  end
  object TrsAtualizaPedidoBanco: TFDTransaction
    Connection = FConexao
    Left = 528
    Top = 384
  end
end
