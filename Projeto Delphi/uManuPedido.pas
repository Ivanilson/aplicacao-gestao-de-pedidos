﻿unit uManuPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Mask, Vcl.DBCtrls,
  Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls;

type

  TItemPedido = class
  private
     Produto    : integer;
     Descricao  : string;
     Quantidade : integer;
     ValorUnitario: Double;
  public
    constructor Create(produto : integer; descricao: string; quantidade: integer ;
                       valorUnitario : double);

    function getProduto(): integer;
    function getDescricao(): String;
    function getQuantidade(): integer;
    function getValorUnitario(): Double;
    procedure Add();
  end;


  TFrmManuPedido = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBGrid2: TDBGrid;
    BbNovo: TButton;
    BbSalvar: TButton;
    BbBuscarProduto: TButton;
    Panel2: TPanel;
    BbFechar: TButton;
    BbSalvarPed: TButton;
    Label6: TLabel;
    EdtEmissao: TDBEdit;
    BbBuscaCliente: TButton;
    LbNomeCli: TLabel;
    EdtProduto: TDBEdit;
    EdtQuantidade: TDBEdit;
    EdtValUnitario: TDBEdit;
    LbProduto: TLabel;
    BbDescartar: TButton;
    EdtValorTotal: TDBEdit;
    Label8: TLabel;
    BbExcPed: TButton;
    LbBusca: TLabel;
    EdtPedido: TEdit;
    BbBuscaPedido: TButton;
    EdtCodCliente: TDBEdit;
    Label9: TLabel;
    BbNovoPed: TButton;
    BbDescartarPed: TButton;
    BbAltPed: TButton;
    EdtNumeroPedido: TDBEdit;
    Label7: TLabel;
    Label10: TLabel;
    procedure BbBuscaClienteClick(Sender: TObject);
    procedure BbBuscarProdutoClick(Sender: TObject);
    procedure BbNovoClick(Sender: TObject);
    procedure BbSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BbSalvarPedClick(Sender: TObject);
    procedure BbFecharClick(Sender: TObject);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BbDescartarClick(Sender: TObject);
    procedure BbBuscaPedidoClick(Sender: TObject);
    procedure BbAltPedClick(Sender: TObject);
    procedure BbExcPedClick(Sender: TObject);
    procedure BbNovoPedClick(Sender: TObject);
    procedure BbDescartarPedClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AtivaEdicao;
    procedure DesativaEdicao;
    procedure DesativaEdicaoItens;
    procedure AtivaEdicaoItens;
    procedure AtualizaTotal;
  end;

var
  FrmManuPedido: TFrmManuPedido;
  Item : TItemPedido;

implementation

{$R *.dfm}

uses uBuscaCliente, uDmGeral;

procedure TFrmManuPedido.AtivaEdicao;
begin
  // Deixa em modo de edição
  BbBuscarProduto.Enabled   := True;
  BbSalvar.Enabled          := True;
  BbDescartar.Enabled       := True;
  BbNovo.Enabled            := True;
  BbNovoPed.Enabled         := False;
  EdtProduto.Enabled        := True;
  EdtQuantidade.Enabled     := True;
  EdtValUnitario.Enabled    := True;

  BbAltPed.Enabled          := False;
  BbExcPed.Enabled          := False;
  BbNovoPed.Enabled         := False;
  BbExcPed.Visible          := False;
  BbBuscaPedido.Visible     := False;
  LbBusca.Visible           := False;
  EdtPedido.Visible         := False;
  BbSalvarPed.Enabled       := True;
  BbDescartarPed.Enabled    := True;

  EdtCodCliente.Enabled     := True;
  BbBuscaCliente.Enabled    := True;
  EdtEmissao.Enabled        := True;

end;

procedure TFrmManuPedido.AtivaEdicaoItens;
begin
  BbSalvar.Enabled       := True;
  BbDescartar.Enabled    := True;
  BbSalvarPed.Enabled    := False;
  BbDescartarPed.Enabled := False;
  BbNovo.Enabled         := False;
  EdtProduto.Enabled     := True;
  EdtQuantidade.Enabled  := True;
  EdtValUnitario.Enabled := True;
  BbBuscarProduto.Enabled:= True;
end;



procedure TFrmManuPedido.DesativaEdicao;
begin
  // Desativa botões e deixa em modo de não edição
  BbBuscarProduto.Enabled   := False;
  BbSalvar.Enabled          := False;
  BbDescartar.Enabled       := False;
  BbNovo.Enabled            := False;
  EdtProduto.Enabled        := False;
  EdtQuantidade.Enabled     := False;
  EdtValUnitario.Enabled    := False;
  EdtProduto.Enabled        := False;
  BbNovoPed.Enabled         := True;
  BbAltPed.Enabled          := True;
  BbExcPed.Enabled          := True;
  BbNovoPed.Enabled         := True;
  BbExcPed.Visible          := False;
  BbBuscaPedido.Visible     := True;
  LbBusca.Visible           := True;
  EdtPedido.Visible         := True;
  BbSalvarPed.Enabled       := False;
  BbDescartarPed.Enabled    := False;
  EdtCodCliente.Enabled     := False;
  BbBuscaCliente.Enabled    := False;
  EdtEmissao.Enabled        := False;
  LbNomeCli.Caption         := '';
  LbProduto.Caption         := '';
end;

procedure TFrmManuPedido.DesativaEdicaoItens;
begin
  BbSalvar.Enabled       := False;
  BbDescartar.Enabled    := False;
  BbSalvarPed.Enabled    := True;
  BbDescartarPed.Enabled := True;
  BbNovo.Enabled         := True;
  EdtProduto.Enabled     := False;
  EdtQuantidade.Enabled  := False;
  EdtValUnitario.Enabled := False;
  BbBuscarProduto.Enabled:= False;
end;

procedure TFrmManuPedido.AtualizaTotal;
var dTotal : double;
begin
  // Soma Valor Total dos Itens
  dTotal := 0;
  // Soma os itens do pedido e atualiza total
  DmGeral.TempItensPedido.First;
  while (not DmGeral.TempItensPedido.Eof ) do
     begin
        dTotal :=  dTotal + DmGeral.TempItensPedidoVALOR_TOTAL.Value;
        DmGeral.TempItensPedido.Next;
     end;
  // Grava campo Valor total do Pedido
  DmGeral.TempPedido.Edit;
  DmGeral.TempPedidoVALOR_TOTAL.Value :=  dTotal;
  DmGeral.TempPedido.Post;

end;

procedure TFrmManuPedido.BbAltPedClick(Sender: TObject);
begin
   // Tenta alterar pedido
   if (not DmGeral.TempPedidoNUMERO_PEDIDO.IsNull ) then
      begin
        DmGeral.TempPedido.Edit;
        AtivaEdicao;
        DesativaEdicaoItens
      end
   else
     begin
      ShowMessage('Pesquise um Pedido para poder alterar!');
      EdtPedido.SetFocus
     end;

end;

procedure TFrmManuPedido.BbBuscaClienteClick(Sender: TObject);
begin
  // Pesquisar Cliente
  Try
    FrmBuscaCliente    :=   TFrmBuscaCliente.Create(Application);
    FrmBuscaCliente.ShowModal;
  Finally
    FrmBuscaCliente.Free;
  End;
  if not DmGeral.TempPedidoCODIGO_CLIENTE.IsNull then
     begin
       AtivaEdicaoItens;
       EdtProduto.setfocus
     end;
end;

procedure TFrmManuPedido.BbBuscarProdutoClick(Sender: TObject);
begin
  if EdtProduto.Text  = '' then
    begin
     Showmessage('Digite o código do produto a ser pesquisado!');
     EdtProduto.SetFocus
    end
  else
    begin
      // Busca o produto pelo código
      With DmGeral.QrBuscaProduto do
        begin
          Close;
          ParamByName('PCODIGO').Value  := DmGeral.TempItensPedidoCODIGO_PRODUTO.Value;
          Open;
        end;
      // Não encontrou exibe mensagem de aviso e seleciona campo
      If DmGeral.QrBuscaProduto.IsEmpty then
        begin
          ShowMessage('Produto não encontrado!');
          DmGeral.TempItensPedidoCODIGO_PRODUTO.Clear;
        end
      else
        begin
          // Libera botão salvar e descartar
          AtivaEdicaoItens;
          // Encontrou então preenche quantidade com 1 e valor unitário com
          // com valor do produto da tabela produtos
          DmGeral.TempItensPedidoQUANTIDADE.Value     := 1;
          DmGeral.TempItensPedidoCODIGO_PRODUTO.Value := DmGeral.QrBuscaProdutoCODIGO_PRODUTO.Value;
          DmGeral.TempItensPedidoDESCRICAO.Value      := DmGeral.QrBuscaProdutoDESCRICAO.Value;
          LbProduto.Caption                           := DmGeral.QrBuscaProdutoDESCRICAO.Value;
          DmGeral.TempItensPedidoVALOR_UNITARIO.Value := DmGeral.QrBuscaProdutoPRECO_VENDA.Value;
          EdtQuantidade.SetFocus
        end;
    end;

end;

procedure TFrmManuPedido.BbDescartarClick(Sender: TObject);
begin
   DmGeral.TempItensPedido.Cancel;
   DesativaEdicaoItens;
end;

procedure TFrmManuPedido.BbDescartarPedClick(Sender: TObject);
begin
  DmGeral.TempItensPedido.EmptyDataSet;
  DmGeral.TempPedido.EmptyDataSet;
  DesativaEdicao;
end;

procedure TFrmManuPedido.BbExcPedClick(Sender: TObject);
begin
   // Excluir Pedido do Banco
   if (not DmGeral.TempPedidoNUMERO_PEDIDO.IsNull)  then
      begin
       if MessageDlg('Confirma exclusão do Pedido?',mtConfirmation,[mbYes,MbNo],0) = mrYes then
         begin

            Try
              DmGeral.FConexao.StartTransaction;

              // Excluir os itens primeiro
              With DmGeral.QrExcluirItemPedidoBanco do
                begin
                   Params[0].Value  :=  DmGeral.TempPedidoNUMERO_PEDIDO.Value;
                   ExecSQL;
                end;

              // Exclui o Pedido
              With DmGeral.QrExcluirPedidoBanco do
                begin
                   Params[0].Value  :=  DmGeral.TempPedidoNUMERO_PEDIDO.Value;
                   ExecSQL;
                end;

              DmGeral.FConexao.Commit;
              ShowMessage('Pedido excluído com sucesso!');
            Except
              begin
                 DmGeral.FConexao.Rollback;
                 ShowMessage('Erro ao Excluir o Pedido!');
              end;
            End;

            DmGeral.TempItensPedido.EmptyDataSet;
            DmGeral.TempPedido.EmptyDataSet;

            BbExcPed.Visible  := False;
          end;

         end;

end;

procedure TFrmManuPedido.BbFecharClick(Sender: TObject);
begin
  Close
end;

procedure TFrmManuPedido.BbNovoClick(Sender: TObject);
begin
  if DmGeral.TempPedidoCODIGO_CLIENTE.IsNull then
    Begin
     ShowMessage('Informe o cliente para criar o pedido antes de inserir os itens!');
     BbBuscaCliente.SetFocus
    End
  else
  if DmGeral.TempPedidoDATAEMISSAO.IsNull then
    Begin
     ShowMessage('Informe a data da Emissão do pedido antes de inserir os itens!');
     EdtEmissao.SetFocus
    End
  else
   begin
     if ( DmGeral.TempPedido.State in [dsInsert] ) then
        // Salva Pedido na temporária antes de iniciar lançar os itens
        DmGeral.TempPedido.Post;


     // Abre novo registro na tabela temporária de itens
     DmGeral.TempItensPedido.Append;

     AtivaEdicaoItens;
     EdtProduto.SetFocus;
   end;
end;

procedure TFrmManuPedido.BbNovoPedClick(Sender: TObject);
begin
  DmGeral.TempPedido.Append;
  AtivaEdicao;
  DesativaEdicaoItens;
end;

procedure TFrmManuPedido.BbSalvarClick(Sender: TObject);
begin
  if DmGeral.TempItensPedidoCODIGO_PRODUTO.IsNull then
    begin
     ShowMessage('Pesquisa o Produto antes de salvar!');
     EdtProduto.SetFocus
    end
  else
    if ( DmGeral.TempItensPedidoQUANTIDADE.IsNull ) or
       ( DmGeral.TempItensPedidoQUANTIDADE.Value <= 0) then
      begin
       ShowMessage('Informe a quantidade!');
       EdtQuantidade.SetFocus
      end
    else
      if ( DmGeral.TempItensPedidoVALOR_UNITARIO.IsNull ) or
         ( DmGeral.TempItensPedidoVALOR_UNITARIO.Value <= 0) then
        begin
         ShowMessage('Informe o valor unitário!');
         EdtValUnitario.SetFocus
        end
       else
         begin
           Item := TItemPedido.Create(DmGeral.TempItensPedidoCODIGO_PRODUTO.Value,
           LbProduto.Caption,
           DmGeral.TempItensPedidoQUANTIDADE.Value,
           DmGeral.TempItensPedidoVALOR_UNITARIO.Value);

           Item.Add;

           // Atualiza Total do Pedido
           AtualizaTotal;

           DesativaEdicaoItens;
         end;

end;

procedure TFrmManuPedido.BbSalvarPedClick(Sender: TObject);
var iUltimoPedido : integer;
begin
   if DmGeral.TempItensPedido.IsEmpty then
      ShowMessage('Insira algum item no pedido antes de salvar!')
   else
     begin
      // Se NÃO TEM campo Numero do Pedido na tabela temporária
      // Então é pedido NOVO gera os INSERTS
      if DmGeral.TempPedidoNUMERO_PEDIDO.IsNull then
        begin
         // Rotina para gravar os dados das tabelas temporárias
         // Abre Transação no banco de dados e inicia try pra capturar
         // qualquer erro e cancelar a transação


         // Recupera ultimo ID do pedido gerado
         With DmGeral.QrNumeroPedido do
           begin
              Close;
              SQL.Clear;
              SQL.Add('SELECT * FROM param_pedido');
              Open;
           end;
         iUltimoPedido :=  DmGeral.QrNumeroPedidoNUMERO_PROX_PEDIDO.Value;
         DmGeral.QrNumeroPedido.Close;

         DmGeral.FConexao.StartTransaction;
         Try
         // Salva Pedido no MySQL
         With DmGeral.QrInserePedido do
            Begin
               Params[0].Value    := iUltimoPedido;
               Params[1].Value    := DmGeral.TempPedidoDATAEMISSAO.Value;
               Params[2].Value    := DmGeral.TempPedidoCODIGO_CLIENTE.Value;
               Params[3].Value    := DmGeral.TempPedidoVALOR_TOTAL.Value;
               ExecSQL;
            End;
          //  DmGeral.TInsPedido.Commit;

            DmGeral.QrIncNumeroPedido.ExecSQL;
       //     DmGeral.TrsIncNumeroPedido.Commit;

            DmGeral.TempItensPedido.First;
            while (not  DmGeral.TempItensPedido.Eof ) do
               begin
                 // Salva Itens do Pedido no MySQL
                 With DmGeral.QrInsereItemPedido do
                    Begin
                       Params[0].Value    := iUltimoPedido;
                       Params[1].Value    := DmGeral.TempItensPedidoCODIGO_PRODUTO.Value;
                       Params[2].Value    := DmGeral.TempItensPedidoQUANTIDADE.Value;
                       Params[3].Value    := DmGeral.TempItensPedidoVALOR_UNITARIO.Value;
                       Params[4].Value    := DmGeral.TempItensPedidoVALOR_TOTAL.Value;
                       ExecSQL;
                    End;
                  DmGeral.TempItensPedido.Next;
               end;
            //   DmGeral.TInsItemPedido.Commit;

           DmGeral.FConexao.Commit;
           ShowMessage('Pedido Número: ' + InttoStr(iUltimoPedido)+ ' cadastrado com sucesso!');
         Except
          begin
            DmGeral.FConexao.Rollback;
            ShowMessage('Erro ao Incluir o Pedido!');
          end;
         End;

         DesativaEdicao;

        end
      else
        begin
          // Está alterando um pedido já salvo

         DmGeral.FConexao.StartTransaction;
         Try
           // Salva Pedido no MySQL
           With DmGeral.QrAtualizaPedidoBanco do
              Begin
                 Params[0].Value    := DmGeral.TempPedidoDATAEMISSAO.Value;
                 Params[1].Value    := DmGeral.TempPedidoCODIGO_CLIENTE.Value;
                 Params[2].Value    := DmGeral.TempPedidoVALOR_TOTAL.Value;
                 Params[3].Value    := DmGeral.TempPedidoNUMERO_PEDIDO.Value;
                 ExecSQL;
              End;

           // Exclui os itens anteriores do pedido no banco
              With DmGeral.QrExcluirItemPedidoBanco do
                Begin
                   Params[0].Value    := DmGeral.TempPedidoNUMERO_PEDIDO.Value;
                   ExecSQL;
                End;

              // Insere os itens atuais do GRID
              DmGeral.TempItensPedido.First;
              while (not  DmGeral.TempItensPedido.Eof ) do
                 begin
                   // Salva Itens do Pedido no MySQL
                   With DmGeral.QrInsereItemPedido do
                      Begin
                         Params[0].Value    := DmGeral.TempPedidoNUMERO_PEDIDO.Value;
                         Params[1].Value    := DmGeral.TempItensPedidoCODIGO_PRODUTO.Value;
                         Params[2].Value    := DmGeral.TempItensPedidoQUANTIDADE.Value;
                         Params[3].Value    := DmGeral.TempItensPedidoVALOR_UNITARIO.Value;
                         Params[4].Value    := DmGeral.TempItensPedidoVALOR_TOTAL.Value;
                         ExecSQL;
                      End;
                    DmGeral.TempItensPedido.Next;
                 end;
              //   DmGeral.TInsItemPedido.Commit;

             DmGeral.FConexao.Commit;
             ShowMessage('Pedido alterado com sucesso!');
         Except
          begin
           DmGeral.FConexao.Rollback;
           ShowMessage('Erro ao salvar o pedido!');
          end;
         End;

         DesativaEdicao;

        end;
     end;
end;

procedure TFrmManuPedido.DBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = VK_RETURN then
  begin
    if ( DmGeral.TempItensPedido.State in [dsBrowse])
    and
       ( not DmGeral.TempItensPedido.IsEmpty) then
      Begin
         // Poe em modo de edição o pedido e o registro
         DmGeral.TempItensPedido.Edit;

         LbProduto.Caption   :=  DmGeral.TempItensPedidoDESCRICAO.Value;


         AtivaEdicao;
         AtivaEdicaoItens;
         EdtQuantidade.SetFocus
      End;

  end;
if Key = VK_DELETE then
  begin
    if ( not DmGeral.TempItensPedido.IsEmpty) then
      Begin
       if MessageDlg('Confirma exclusão do item do pedido?',mtConfirmation,[mbYes,MbNo],0) = mrYes then
         begin
           // Poe em modo de edição o registro
           DmGeral.TempItensPedido.Delete;
           AtualizaTotal;
           AtivaEdicao;
           DesativaEdicaoItens;
         end;
      End;

  end;

end;

procedure TFrmManuPedido.BbBuscaPedidoClick(Sender: TObject);
begin
  if EdtPedido.Text = '' then
     ShowMessage('Informe o número do Pedido!')
  else
    begin
  // Busca Pedido salvo
  DmGeral.QrPedido.Close;
  With DmGeral.QrPedido do
    Begin
       SQL.Clear;
       SQL.Add('SELECT PEDIDO.NUMERO_PEDIDO, PEDIDO.DATA_EMISSAO, PEDIDO.CODIGO_CLIENTE,');
       SQL.Add('CLIENTES.NOME, PEDIDO.VALOR_TOTAL');
       SQL.Add('FROM pedidos_dados_gerais PEDIDO');
       SQL.Add('INNER JOIN clientes ON (PEDIDO.CODIGO_CLIENTE = clientes.CODIGO_CLIENTE)');
       SQL.Add('WHERE PEDIDO.NUMERO_PEDIDO = :PNUMERO ');
       ParamByName('PNUMERO').Value   :=   StrToInt(EdtPedido.Text);
       Open;
    End;
    if DmGeral.QrPedido.IsEmpty then
       ShowMessage('Pedido não encontrado!')
    else
      begin
        // Habilita opção EXCLUIR

        BbExcPed.Visible  := True;

        DmGeral.QrItensPedido.Open;
        // Carrega os dados do pedido nas tabelas temporárias

        // Pedido
        DmGeral.TempPedido.Append;
        DmGeral.TempPedidoNUMERO_PEDIDO.Value   := DmGeral.QrPedidoNUMERO_PEDIDO.Value;
        DmGeral.TempPedidoCODIGO_CLIENTE.Value  := DmGeral.QrPedidoCODIGO_CLIENTE.Value;
        DmGeral.TempPedidoDATAEMISSAO.Value     := DmGeral.QrPedidoDATA_EMISSAO.Value;
        DmGeral.TempPedidoVALOR_TOTAL.Value     := DmGeral.QrPedidoVALOR_TOTAL.Value;
        DmGeral.TempPedidoNOMECLIENTE.Value     := DmGeral.QrPedidoNOME.Value;
        LbNomeCli.Caption                       := DmGeral.QrPedidoNOME.Value;
        DmGeral.TempPedido.Post;

        // Itens do Pedido
        DmGeral.QrItensPedido.First;

        while (not DmGeral.QrItensPedido.Eof ) do
          begin

            DmGeral.TempItensPedido.Append;
            DmGeral.TempItensPedidoNUMERO_PEDIDO.Value   := DmGeral.QrItensPedidoNUMERO_PEDIDO.Value;
            DmGeral.TempItensPedidoDESCRICAO.Value       := DmGeral.QrItensPedidoDESCRICAO.Value;
            DmGeral.TempItensPedidoCODIGO_PRODUTO.Value  := DmGeral.QrItensPedidoCODIGO_PRODUTO.Value;
            DmGeral.TempItensPedidoQUANTIDADE.Value      := DmGeral.QrItensPedidoQUANTIDADE.Value;
            DmGeral.TempItensPedidoVALOR_UNITARIO.Value  := DmGeral.QrItensPedidoVALOR_UNITARIO.Value;
            DmGeral.TempItensPedidoVALOR_TOTAL.Value     := DmGeral.QrItensPedidoVALOR_TOTAL.Value;
            DmGeral.TempItensPedido.Post;

            DmGeral.QrItensPedido.Next;
          end;

      end;

    end;


end;

procedure TFrmManuPedido.FormShow(Sender: TObject);
begin
  Caption                   := 'Novo Pedido';
  LbProduto.Caption         := '';
  LbNomeCli.Caption         := '';


  // Limpa tabelas temporárias
  DmGeral.TempPedido.EmptyDataSet;
  DmGeral.TempItensPedido.EmptyDataSet;

  DesativaEdicao;
end;


procedure TItemPedido.Add;
begin
  // Salva item do pedido
  DmGeral.TempItensPedidoVALOR_TOTAL.Value :=  DmGeral.TempItensPedidoQUANTIDADE.Value *
  DmGeral.TempItensPedidoVALOR_UNITARIO.Value;
  // Item.ValorUnitario*
 //  Item.Quantidade;
  DmGeral.TempItensPedidoDESCRICAO.Value   := FrmManuPedido.LbProduto.Caption;
  //Item.getDescricao;
  DmGeral.TempItensPedido.Post;
end;

constructor TItemPedido.Create(produto : integer; descricao: string; quantidade: integer ;
    valorunitario : double);
begin
  produto           := DmGeral.TempItensPedidoCODIGO_PRODUTO.Value;
  descricao         := FrmManuPedido.LbProduto.Caption;
  quantidade        := DmGeral.TempItensPedidoQUANTIDADE.Value;
  valorunitario     := DmGeral.TempItensPedidoVALOR_UNITARIO.Value;
end;


function TItemPedido.getDescricao: String;
begin
  getDescricao := Descricao;
end;

function TItemPedido.getProduto: integer;
begin
  getProduto  := Produto;
end;

function TItemPedido.getQuantidade: integer;
begin
  getQuantidade := quantidade
end;

function TItemPedido.getValorUnitario: Double;
begin
 getValorUnitario := valorunitario
end;


end.
