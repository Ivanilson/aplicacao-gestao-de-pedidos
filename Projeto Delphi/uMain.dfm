object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'Gest'#227'o de Pedidos - 1.0'
  ClientHeight = 379
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 640
    Height = 379
    Align = alClient
    Caption = 'Sistema  de Gest'#227'o de Pedidos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -37
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 152
    ExplicitTop = 64
    ExplicitWidth = 185
    ExplicitHeight = 41
  end
  object MainMenu1: TMainMenu
    Left = 72
    Top = 112
    object Arquivo1: TMenuItem
      Caption = 'Arquivo'
      Checked = True
      object CadastrodePedidos1: TMenuItem
        Action = Action1
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Sair1: TMenuItem
        Action = Action2
      end
    end
    object Ajuda1: TMenuItem
      Caption = 'Ajuda'
      object Sobre1: TMenuItem
        Action = Action3
      end
    end
  end
  object ActionList1: TActionList
    Left = 72
    Top = 184
    object Action1: TAction
      Caption = 'Pedidos de Venda'
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = 'Sair'
      OnExecute = Action2Execute
    end
    object Action3: TAction
      Caption = 'Sobre'
      OnExecute = Action3Execute
    end
  end
end
