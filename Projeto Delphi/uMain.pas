unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Db, System.Actions,
  Vcl.ActnList, Vcl.ExtCtrls;

type
  TFrmMain = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Ajuda1: TMenuItem;
    Sobre1: TMenuItem;
    CadastrodePedidos1: TMenuItem;
    N1: TMenuItem;
    Sair1: TMenuItem;
    ActionList1: TActionList;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Panel1: TPanel;
    procedure FormShow(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation

{$R *.dfm}

uses uDmGeral, uPedidos, uSobre;

procedure TFrmMain.Action1Execute(Sender: TObject);
begin
  // Mostra tela Pedidos
  Try
    FrmPedidos  := TFrmPedidos.Create(Application);
    FrmPedidos.ShowModal;
  Finally
    FrmPedidos.Free;
  End;
end;

procedure TFrmMain.Action2Execute(Sender: TObject);
begin
  // Sair do sistema
  Close
end;

procedure TFrmMain.Action3Execute(Sender: TObject);
begin
  // Mostra o form Sobre
  Try
    FrmAboutBox   := TFrmAboutBox.Create(Application);
    FrmAboutBox.ShowModal;
  Finally
    FrmAboutBox.Free;
  End;

end;

procedure TFrmMain.FormShow(Sender: TObject);
var
  FormatBr: TFormatSettings;
begin

   // Formata o ambiente

   FormatBr                     := TFormatSettings.Create;
   FormatBr.DecimalSeparator    := ',';
   FormatBr.ThousandSeparator   := '.';
   FormatBr.CurrencyDecimals    := 2;
   FormatBr.DateSeparator       := '/';
   FormatBr.ShortDateFormat     := 'dd/mm/yyyy';
   FormatBr.LongDateFormat      := 'dd/mm/yyyy';
   FormatBr.TimeSeparator       := ':';
   FormatBr.TimeAMString        := 'AM';
   FormatBr.TimePMString        := 'PM';
   FormatBr.ShortTimeFormat     := 'hh:nn';
   FormatBr.LongTimeFormat      := 'hh:nn:ss';
   FormatBr.CurrencyString      := 'R$';

   System.SysUtils.FormatSettings := FormatBr;

   // Tenta conex�o com banco de dados usando FDAC
   // Usuario: root e senha: em branco
   // Para alterar a senha e usu�rio do Mysql de sua instala��o altere aqui

   DmGeral.FConexao.Params.Clear;
   DmGeral.FConexao.Params.Add('Database=wk');
   DmGeral.FConexao.Params.Add('User_Name=root');
   DmGeral.FConexao.Params.Add('Password=');
   DmGeral.FConexao.Params.Add('DriverID=MySQL');

   // Tenta conex�o
   Try
     DmGeral.FConexao.Connected := True;
   Except
     On E: EDatabaseError do
       ShowMessage('Erro na conex�o ao banco de dados. Mensagem:'+ E.message);
   End;
end;

end.
