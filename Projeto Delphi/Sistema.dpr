program Sistema;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {FrmMain},
  uDmGeral in 'uDmGeral.pas' {DmGeral: TDataModule},
  uPedidos in 'uPedidos.pas' {FrmPedidos},
  uBuscaCliente in 'uBuscaCliente.pas' {FrmBuscaCliente},
  uSobre in 'uSobre.pas' {FrmAboutBox},
  uManuPedido in 'uManuPedido.pas' {FrmManuPedido};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDmGeral, DmGeral);
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
