object FrmBuscaCliente: TFrmBuscaCliente
  Left = 0
  Top = 0
  Caption = 'FrmBuscaCliente'
  ClientHeight = 368
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 814
    Height = 304
    Align = alClient
    DataSource = DmGeral.DsBuscaCliente
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CODIGO_CLIENTE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CIDADE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 304
    Width = 814
    Height = 64
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      814
      64)
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 179
      Height = 19
      Caption = 'Digite o nome do cliente:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdtNome: TEdit
      Left = 200
      Top = 16
      Width = 297
      Height = 27
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object BbPesquisar: TButton
      AlignWithMargins = True
      Left = 508
      Top = 14
      Width = 91
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Pesquisar'
      TabOrder = 1
      OnClick = BbPesquisarClick
    end
    object BbSelecionar: TButton
      AlignWithMargins = True
      Left = 615
      Top = 14
      Width = 91
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Selecionar'
      TabOrder = 2
      OnClick = BbSelecionarClick
    end
    object BbFechar: TButton
      AlignWithMargins = True
      Left = 722
      Top = 14
      Width = 91
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Fechar'
      TabOrder = 3
      OnClick = BbFecharClick
    end
  end
end
