object FrmPedidos: TFrmPedidos
  Left = 0
  Top = 0
  Caption = 'FrmPedidos'
  ClientHeight = 450
  ClientWidth = 789
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 789
    Height = 409
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 224
      Height = 19
      Caption = 'Listagem dos Pedidos de Venda'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 199
      Width = 111
      Height = 19
      Caption = 'Itens do Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 48
      Width = 721
      Height = 129
      DataSource = DmGeral.DsPedido
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NUMERO_PEDIDO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATA_EMISSAO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CODIGO_CLIENTE'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'NOME'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR_TOTAL'
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 16
      Top = 224
      Width = 761
      Height = 161
      DataSource = DmGeral.DsItens
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnKeyDown = DBGrid2KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'CODIGO_ITEM'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'NUMERO_PEDIDO'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'CODIGO_PRODUTO'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'DESCRICAO'
          Width = 301
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTIDADE'
          Width = 109
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR_UNITARIO'
          Width = 116
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR_TOTAL'
          Width = 116
          Visible = True
        end>
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 409
    Width = 789
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      789
      41)
    object BbFechar: TButton
      AlignWithMargins = True
      Left = 686
      Top = 6
      Width = 91
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Fechar'
      TabOrder = 0
      OnClick = BbFecharClick
    end
    object BbNovoPedido: TButton
      AlignWithMargins = True
      Left = 23
      Top = 6
      Width = 202
      Height = 25
      Anchors = []
      Caption = '&Manuten'#231#227'o de Pedidos'
      TabOrder = 1
      OnClick = BbNovoPedidoClick
    end
  end
end
