-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for wk
CREATE DATABASE IF NOT EXISTS `wk` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `wk`;

-- Dumping structure for table wk.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `CODIGO_CLIENTE` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(50) NOT NULL DEFAULT '',
  `CIDADE` varchar(50) NOT NULL DEFAULT '',
  `UF` varchar(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`CODIGO_CLIENTE`),
  FULLTEXT KEY `NOME` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COMMENT='Tabela de Clientes';

-- Dumping data for table wk.clientes: ~20 rows (approximately)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`CODIGO_CLIENTE`, `NOME`, `CIDADE`, `UF`) VALUES
	(1, 'MARIA', 'MANAUS', 'AM'),
	(2, 'JOAO', 'RIO DE JANEIRO', 'RJ'),
	(3, 'MARIA', 'MANAUS', 'AM'),
	(4, 'JOAO', 'RIO BRANCO', 'AC'),
	(5, 'PEDRO', 'BELO HORIZONTE', 'MG'),
	(6, 'JACINTO', 'SANTOS', 'SP'),
	(7, 'JULIA', 'RIO GRANDE DO SUL', 'RS'),
	(8, 'ANA', 'RIO DE JANEIRO', 'RJ'),
	(9, 'MARGARIDA', 'RIO DE JANEIRO', 'RJ'),
	(10, 'BEATRIZ', 'RIO GRANDE DO SUL', 'RS'),
	(11, 'JULIANA', 'RIO DE JANEIRO', 'RJ'),
	(12, 'ANDERSON', 'RIO GRANDE DO SUL', 'RS'),
	(13, 'JOANA', 'RIO DE JANEIRO', 'RJ'),
	(14, 'JOAO', 'SANTOS', 'SP'),
	(15, 'DEREK', 'RIO GRANDE DO SUL', 'RS'),
	(16, 'ROBERT', 'RIO DE JANEIRO', 'RJ'),
	(17, 'PATRICK', 'SANTOS', 'SP'),
	(18, 'CLEBER', 'RIO GRANDE DO SUL', 'RS'),
	(19, 'AMANDA', 'RIO DE JANEIRO', 'RJ'),
	(20, 'ALEXANDRE', 'RIO GRANDE DO SUL', 'RS');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Dumping structure for table wk.param_pedido
CREATE TABLE IF NOT EXISTS `param_pedido` (
  `NUMERO_PROX_PEDIDO` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela parametrização pedidos';

-- Dumping data for table wk.param_pedido: ~1 rows (approximately)
/*!40000 ALTER TABLE `param_pedido` DISABLE KEYS */;
INSERT INTO `param_pedido` (`NUMERO_PROX_PEDIDO`) VALUES
	(3);
/*!40000 ALTER TABLE `param_pedido` ENABLE KEYS */;

-- Dumping structure for table wk.pedidos_dados_gerais
CREATE TABLE IF NOT EXISTS `pedidos_dados_gerais` (
  `NUMERO_PEDIDO` int(11) NOT NULL,
  `DATA_EMISSAO` date NOT NULL,
  `CODIGO_CLIENTE` int(11) NOT NULL,
  `VALOR_TOTAL` decimal(12,2) NOT NULL,
  PRIMARY KEY (`NUMERO_PEDIDO`),
  KEY `FK_pedidos_dados_gerais_clientes` (`CODIGO_CLIENTE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela Master de Pedidos';

-- Dumping data for table wk.pedidos_dados_gerais: ~2 rows (approximately)
/*!40000 ALTER TABLE `pedidos_dados_gerais` DISABLE KEYS */;
INSERT INTO `pedidos_dados_gerais` (`NUMERO_PEDIDO`, `DATA_EMISSAO`, `CODIGO_CLIENTE`, `VALOR_TOTAL`) VALUES
	(1, '2021-02-17', 20, 19.00),
	(2, '2021-02-17', 15, 122.00);
/*!40000 ALTER TABLE `pedidos_dados_gerais` ENABLE KEYS */;

-- Dumping structure for table wk.pedidos_produtos
CREATE TABLE IF NOT EXISTS `pedidos_produtos` (
  `CODIGO_ITEM` int(11) NOT NULL AUTO_INCREMENT,
  `NUMERO_PEDIDO` int(11) NOT NULL,
  `CODIGO_PRODUTO` int(11) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `VALOR_UNITARIO` decimal(12,2) NOT NULL,
  `VALOR_TOTAL` decimal(12,2) NOT NULL,
  PRIMARY KEY (`CODIGO_ITEM`),
  KEY `FK_pedidos_produtos_produtos` (`CODIGO_PRODUTO`),
  KEY `FK_pedidos_produtos_pedidos_dados_gerais` (`NUMERO_PEDIDO`),
  CONSTRAINT `FK_pedidos_produtos_pedidos_dados_gerais` FOREIGN KEY (`NUMERO_PEDIDO`) REFERENCES `pedidos_dados_gerais` (`NUMERO_PEDIDO`),
  CONSTRAINT `FK_pedidos_produtos_produtos` FOREIGN KEY (`CODIGO_PRODUTO`) REFERENCES `produtos` (`CODIGO_PRODUTO`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 COMMENT='Tabela dos itens do pedido';

-- Dumping data for table wk.pedidos_produtos: ~5 rows (approximately)
/*!40000 ALTER TABLE `pedidos_produtos` DISABLE KEYS */;
INSERT INTO `pedidos_produtos` (`CODIGO_ITEM`, `NUMERO_PEDIDO`, `CODIGO_PRODUTO`, `QUANTIDADE`, `VALOR_UNITARIO`, `VALOR_TOTAL`) VALUES
	(17, 1, 1, 1, 10.00, 10.00),
	(18, 1, 2, 1, 9.00, 9.00),
	(19, 2, 5, 1, 5.00, 5.00),
	(20, 2, 9, 1, 29.00, 29.00),
	(21, 2, 3, 11, 8.00, 88.00);
/*!40000 ALTER TABLE `pedidos_produtos` ENABLE KEYS */;

-- Dumping structure for table wk.produtos
CREATE TABLE IF NOT EXISTS `produtos` (
  `CODIGO_PRODUTO` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(50) NOT NULL,
  `PRECO_VENDA` decimal(12,2) NOT NULL,
  PRIMARY KEY (`CODIGO_PRODUTO`),
  FULLTEXT KEY `DESCRICAO` (`DESCRICAO`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COMMENT='Tabela de Produtos';

-- Dumping data for table wk.produtos: ~20 rows (approximately)
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` (`CODIGO_PRODUTO`, `DESCRICAO`, `PRECO_VENDA`) VALUES
	(1, 'PRODUTO 1', 10.00),
	(2, 'PRODUTO 2', 9.00),
	(3, 'PRODUTO 3', 8.00),
	(4, 'PRODUTO 4', 4.00),
	(5, 'PRODUTO 5', 5.00),
	(6, 'PRODUTO 6', 8.00),
	(7, 'PRODUTO 7', 9.00),
	(8, 'PRODUTO 8', 19.00),
	(9, 'PRODUTO 9', 29.00),
	(10, 'PRODUTO 10', 21.00),
	(11, 'PRODUTO 11', 29.00),
	(12, 'PRODUTO 12', 15.00),
	(13, 'PRODUTO 13', 22.00),
	(14, 'PRODUTO 14', 49.00),
	(15, 'PRODUTO 15', 11.00),
	(16, 'PRODUTO 16', 9.00),
	(17, 'PRODUTO 17', 18.00),
	(18, 'PRODUTO 18', 20.00),
	(19, 'PRODUTO 19', 22.00),
	(20, 'PRODUTO 20', 13.00);
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
